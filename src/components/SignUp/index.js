import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Field, reduxForm } from "redux-form";
//import Modal from "../Modal/Modal";
const validate = values => {
  const errors = {};
  if (!values.email) {
    errors.email = "Email is Required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Invalid email address";
  }
  if (!values.password) {
    errors.email = "Password is Required";
  } else if (values.password.length <= 5) {
    errors.password = "Password must be 6 or more character long";
  }
  if (!values.email && !values.password) {
    errors.email = "Email & Password are Required!";
  }
  return errors;
};

class SignUp extends Component {
  //   state = {
  //     modal: false
  //   };

  //   toggle = () => {
  //     this.setState({
  //       modal: !this.state.modal
  //     });
  //   };
  renderField = ({
    input,
    label,
    type,
    name,
    meta: { touched, error, warning }
  }) => (
    <div>
      <div>
        <input {...input} placeholder={label} name={name} type={type} />
        <br />
        {touched &&
          ((error && <span style={{ color: "red" }}>{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    </div>
  );

  handleSignupForm = values => {
    //console.log("email:", values.email);
  };
  render() {
    const { handleSubmit, pristine, reset, submitting } = this.props;
    return (
      <>
        <center>
          <br />
          <h1>Sign Up</h1>
          <br />
          {/* <Modal
            modal={this.state.modal}
            toggle={this.toggle}
            handleSendForm={this.handleSignupForm}
          /> */}
          <form onSubmit={handleSubmit(this.handleSignupForm)}>
            <Field
              name="email"
              type="email"
              component={this.renderField}
              label="Email"
            />
            <br />
            <Field
              name="password"
              type="password"
              component={this.renderField}
              label="Password"
            />
            <br />
            <div>
              <button type="submit" disabled={submitting}>
                Submit
              </button>
              <button
                type="button"
                disabled={pristine || submitting}
                onClick={reset}
              >
                Clear
              </button>
            </div>
            <h4>
              Already have account? <Link to="/login">Log In</Link>
            </h4>
          </form>
        </center>
      </>
    );
  }
}

SignUp = reduxForm({
  form: "signUpForm",
  validate
})(SignUp);

export default connect(
  null,
  null
)(SignUp);
