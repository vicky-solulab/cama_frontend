import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import SignUp from "./components/SignUp";
import SignIn from "./components/SignIn";

class App extends Component {
  render() {
    return (
      <center>
        <Switch>
          <Route path="/login" component={SignIn} />
          <Route path="/signup" component={SignUp} />
          <Route path="/" component={SignUp} />
        </Switch>
      </center>
    );
  }
}

export default App;
